[Ivy]
1654696FC4E68524 3.20 #module
>Proto >Proto Collection #zClass
Ps0 Process Big #zClass
Ps0 B #cInfo
Ps0 #process
Ps0 @TextInP .resExport .resExport #zField
Ps0 @TextInP .type .type #zField
Ps0 @TextInP .processKind .processKind #zField
Ps0 @AnnotationInP-0n ai ai #zField
Ps0 @MessageFlowInP-0n messageIn messageIn #zField
Ps0 @MessageFlowOutP-0n messageOut messageOut #zField
Ps0 @TextInP .xml .xml #zField
Ps0 @TextInP .responsibility .responsibility #zField
Ps0 @StartRequest f0 '' #zField
Ps0 @EndTask f1 '' #zField
Ps0 @RichDialog f8 '' #zField
Ps0 @TaskSwitchSimple f15 '' #zField
Ps0 @RichDialog f16 '' #zField
Ps0 @TkArc f17 '' #zField
Ps0 @EMail f7 '' #zField
Ps0 @PushWFArc f14 '' #zField
Ps0 @PushWFArc f6 '' #zField
Ps0 @RichDialog f19 '' #zField
Ps0 @Alternative f11 '' #zField
Ps0 @PushWFArc f22 '' #zField
Ps0 @PushWFArc f18 '' #zField
Ps0 @EMail f23 '' #zField
Ps0 @PushWFArc f24 '' #zField
Ps0 @PushWFArc f25 '' #zField
Ps0 @PushWFArc f4 '' #zField
Ps0 @RichDialog f3 '' #zField
Ps0 @PushWFArc f2 '' #zField
Ps0 @PushWFArc f10 '' #zField
Ps0 @Alternative f12 '' #zField
Ps0 @PushWFArc f21 '' #zField
Ps0 @PushWFArc f20 '' #zField
>Proto Ps0 Ps0 Process #zField
Ps0 f0 outLink start.ivp #txt
Ps0 f0 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f0 inParamDecl '<> param;' #txt
Ps0 f0 actionDecl 'ivy.advanced.exercise3.ProcessData out;
' #txt
Ps0 f0 guid 1654696FC5F11EDF #txt
Ps0 f0 requestEnabled true #txt
Ps0 f0 triggerEnabled false #txt
Ps0 f0 callSignature start() #txt
Ps0 f0 caseData businessCase.attach=true #txt
Ps0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
<language>
<name>start.ivp</name>
</language>
</elementInfo>
' #txt
Ps0 f0 @C|.responsibility Everybody #txt
Ps0 f0 49 177 30 30 -21 17 #rect
Ps0 f0 @|StartRequestIcon #fIcon
Ps0 f1 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f1 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
<language>
<name>end</name>
<nameStyle>3,7
</nameStyle>
</language>
</elementInfo>
' #txt
Ps0 f1 945 177 30 30 -10 -35 #rect
Ps0 f1 @|EndIcon #fIcon
Ps0 f8 targetWindow NEW #txt
Ps0 f8 targetDisplay TOP #txt
Ps0 f8 richDialogId ivy.advanced.exercise3.LeavingRequestDialog #txt
Ps0 f8 startMethod start(ivy.advanced.exercise3.UserInformation) #txt
Ps0 f8 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f8 requestActionDecl '<ivy.advanced.exercise3.UserInformation userInformation> param;' #txt
Ps0 f8 requestMappingAction 'param.userInformation=in.userInformation;
' #txt
Ps0 f8 responseActionDecl 'ivy.advanced.exercise3.ProcessData out;
' #txt
Ps0 f8 responseMappingAction 'out=in;
out.requestInformation=result.requestInformation;
' #txt
Ps0 f8 isAsynch false #txt
Ps0 f8 isInnerRd false #txt
Ps0 f8 userContext '* ' #txt
Ps0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Call LeavingRequest dialog</name>
        <nameStyle>26,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f8 416 74 160 44 -76 -8 #rect
Ps0 f8 @|RichDialogIcon #fIcon
Ps0 f15 actionDecl 'ivy.advanced.exercise3.ProcessData out;
' #txt
Ps0 f15 actionTable 'out.requestInformation=in2.requestInformation;
out.userInformation=in2.userInformation;
' #txt
Ps0 f15 outTypes "ivy.advanced.exercise3.ProcessData" #txt
Ps0 f15 outLinks "TaskA.ivp" #txt
Ps0 f15 taskData 'TaskA.EXPRI=2
TaskA.EXROL=Everybody
TaskA.EXTYPE=0
TaskA.NAM=<%\=in2.userInformation.fullName%>
TaskA.PRI=2
TaskA.ROL=Superior
TaskA.SKIP_TASK_LIST=false
TaskA.TYPE=0' #txt
Ps0 f15 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f15 template "" #txt
Ps0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Approve leaving request</name>
        <nameStyle>23,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f15 481 177 30 30 -63 -37 #rect
Ps0 f15 @|TaskSwitchSimpleIcon #fIcon
Ps0 f16 targetWindow NEW #txt
Ps0 f16 targetDisplay TOP #txt
Ps0 f16 richDialogId ivy.advanced.exercise3.HandleRequestDialog #txt
Ps0 f16 startMethod start(ivy.advanced.exercise3.RequestInformation,ivy.advanced.exercise3.UserInformation) #txt
Ps0 f16 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f16 requestActionDecl '<ivy.advanced.exercise3.RequestInformation requestInformation, ivy.advanced.exercise3.UserInformation userInformation> param;' #txt
Ps0 f16 requestMappingAction 'param.requestInformation=in.requestInformation;
param.userInformation=in.userInformation;
' #txt
Ps0 f16 responseActionDecl 'ivy.advanced.exercise3.ProcessData out;
' #txt
Ps0 f16 responseMappingAction 'out=in;
out.requestInformation=result.requestInformation;
' #txt
Ps0 f16 isAsynch false #txt
Ps0 f16 isInnerRd false #txt
Ps0 f16 userContext '* ' #txt
Ps0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Call HandleRequest dialog</name>
        <nameStyle>25,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f16 416 266 160 44 -74 -8 #rect
Ps0 f16 @|RichDialogIcon #fIcon
Ps0 f17 expr out #txt
Ps0 f17 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f17 var in2 #txt
Ps0 f17 496 118 496 177 #arcP
Ps0 f7 beanConfig '"{/emailSubject ""Leaving Request Approval""/emailFrom ""<%=in.userInformation.email%>""/emailReplyTo """"/emailTo ""<%=in.userInformation.email%>""/emailCC """"/emailBCC """"/exceptionMissingEmailAttachments ""false""/emailMessage ""Dear <%=in.userInformation.fullName%>,\\n\\nYour leaving request on [<%=in.requestInformation.startDate%> to <%=in.requestInformation.endDate%>] has been approved by superior.\\n\\nBest reagards,\\nSuperior.""/emailAttachments * }"' #txt
Ps0 f7 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f7 timeout 0 #txt
Ps0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Send approving email</name>
        <nameStyle>20,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f7 768 234 128 44 -60 -8 #rect
Ps0 f7 @|EMailIcon #fIcon
Ps0 f14 expr data #txt
Ps0 f14 outCond ivp=="TaskA.ivp" #txt
Ps0 f14 496 207 496 266 #arcP
Ps0 f6 expr out #txt
Ps0 f6 896 256 960 207 #arcP
Ps0 f6 1 960 256 #addKink
Ps0 f6 0 0.816890043944876 0 0 #arcLabel
Ps0 f19 targetWindow NEW #txt
Ps0 f19 targetDisplay TOP #txt
Ps0 f19 richDialogId ivy.advanced.exercise3.ManageRequestsDialog #txt
Ps0 f19 startMethod start(ivy.advanced.exercise3.LoginInformation,ivy.advanced.exercise3.UserInformation) #txt
Ps0 f19 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f19 requestActionDecl '<ivy.advanced.exercise3.LoginInformation loginInformation, ivy.advanced.exercise3.UserInformation userInformation> param;' #txt
Ps0 f19 requestMappingAction 'param.loginInformation=in.loginInformation;
param.userInformation=in.userInformation;
' #txt
Ps0 f19 responseActionDecl 'ivy.advanced.exercise3.ProcessData out;
' #txt
Ps0 f19 responseMappingAction 'out=in;
' #txt
Ps0 f19 isAsynch false #txt
Ps0 f19 isInnerRd false #txt
Ps0 f19 userContext '* ' #txt
Ps0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Call ManageRequests dialog</name>
        <nameStyle>26,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f19 480 362 176 44 -80 -8 #rect
Ps0 f19 @|RichDialogIcon #fIcon
Ps0 f11 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Is approved?</name>
        <nameStyle>12,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f11 640 272 32 32 -56 -33 #rect
Ps0 f11 @|AlternativeIcon #fIcon
Ps0 f22 expr out #txt
Ps0 f22 576 288 640 288 #arcP
Ps0 f22 0 0.3582929055917258 0 0 #arcLabel
Ps0 f18 expr in #txt
Ps0 f18 outCond in.requestInformation.isApproved #txt
Ps0 f18 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Yes</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f18 666 282 768 256 #arcP
Ps0 f18 1 704 256 #addKink
Ps0 f18 1 0.4375 0 -9 #arcLabel
Ps0 f23 beanConfig '"{/emailSubject ""Leaving Request Rejection""/emailFrom ""<%=in.userInformation.email%>""/emailReplyTo """"/emailTo ""<%=in.userInformation.email%>""/emailCC """"/emailBCC """"/exceptionMissingEmailAttachments ""false""/emailMessage ""Dear <%=in.userInformation.fullName%>,\\n\\nYour leaving request on [<%=in.requestInformation.startDate%> to <%=in.requestInformation.endDate%>] has been rejected by superior.\\n\\nBest reagards,\\nSuperior.""/emailAttachments * }"' #txt
Ps0 f23 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f23 timeout 0 #txt
Ps0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Send rejecting email</name>
        <nameStyle>20,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f23 768 298 128 44 -56 -8 #rect
Ps0 f23 @|EMailIcon #fIcon
Ps0 f24 expr in #txt
Ps0 f24 outCond !in.requestInformation.isApproved #txt
Ps0 f24 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>No</name>
        <nameStyle>2,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f24 666 294 768 320 #arcP
Ps0 f24 1 704 320 #addKink
Ps0 f24 1 0.390625 0 -9 #arcLabel
Ps0 f25 expr out #txt
Ps0 f25 896 320 960 207 #arcP
Ps0 f25 1 960 320 #addKink
Ps0 f25 1 0.9620217220913487 0 0 #arcLabel
Ps0 f4 expr out #txt
Ps0 f4 79 192 136 192 #arcP
Ps0 f3 targetWindow NEW #txt
Ps0 f3 targetDisplay TOP #txt
Ps0 f3 richDialogId ivy.advanced.exercise3.LoginDialog #txt
Ps0 f3 startMethod start() #txt
Ps0 f3 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f3 requestActionDecl '<> param;' #txt
Ps0 f3 responseActionDecl 'ivy.advanced.exercise3.ProcessData out;
' #txt
Ps0 f3 responseMappingAction 'out=in;
out.loginInformation=result.loginInformation;
out.userInformation=result.userInformation;
' #txt
Ps0 f3 isAsynch false #txt
Ps0 f3 isInnerRd false #txt
Ps0 f3 userContext '* ' #txt
Ps0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
<language>
<name>Call Login dialog</name>
<nameStyle>17,7
</nameStyle>
</language>
</elementInfo>
' #txt
Ps0 f3 136 170 112 44 -46 -8 #rect
Ps0 f3 @|RichDialogIcon #fIcon
Ps0 f2 expr out #txt
Ps0 f2 248 192 304 192 #arcP
Ps0 f10 expr in #txt
Ps0 f10 outCond in.userInformation.role.equalsIgnoreCase("Employee") #txt
Ps0 f10 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
<language>
<name>Employee</name>
<nameStyle>8,7
</nameStyle>
</language>
</elementInfo>
' #txt
Ps0 f10 320 176 416 96 #arcP
Ps0 f10 1 320 96 #addKink
Ps0 f10 0 0.5125 29 0 #arcLabel
Ps0 f12 type ivy.advanced.exercise3.ProcessData #txt
Ps0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
<language>
<name>What role?</name>
<nameStyle>10,7
</nameStyle>
</language>
</elementInfo>
' #txt
Ps0 f12 304 176 32 32 -29 18 #rect
Ps0 f12 @|AlternativeIcon #fIcon
Ps0 f21 expr out #txt
Ps0 f21 656 384 960 207 #arcP
Ps0 f21 1 960 384 #addKink
Ps0 f21 0 0.04191291667348251 26 1 #arcLabel
Ps0 f20 expr in #txt
Ps0 f20 outCond in.userInformation.role.equalsIgnoreCase("Superior") #txt
Ps0 f20 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
<language>
<name>Superior</name>
<nameStyle>8,7
</nameStyle>
</language>
</elementInfo>
' #txt
Ps0 f20 320 208 480 384 #arcP
Ps0 f20 1 320 384 #addKink
Ps0 f20 0 0.3181818181818182 25 0 #arcLabel
>Proto Ps0 .type ivy.advanced.exercise3.ProcessData #txt
>Proto Ps0 .processKind NORMAL #txt
>Proto Ps0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel>Leaving request</swimlaneLabel>
        <swimlaneLabel>Eployee</swimlaneLabel>
        <swimlaneLabel>Superior</swimlaneLabel>
    </language>
    <swimlaneOrientation>false</swimlaneOrientation>
    <swimlaneSize>384</swimlaneSize>
    <swimlaneSize>160</swimlaneSize>
    <swimlaneSize>224</swimlaneSize>
    <swimlaneColor gradient="false">-3342388</swimlaneColor>
    <swimlaneColor gradient="false">-52</swimlaneColor>
    <swimlaneColor gradient="false">-3342337</swimlaneColor>
    <swimlaneType>POOL</swimlaneType>
    <swimlaneType>LANE_IN_POOL</swimlaneType>
    <swimlaneType>LANE_IN_POOL</swimlaneType>
    <swimlaneSpaceBefore>32</swimlaneSpaceBefore>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
    <swimlaneSpaceBefore>0</swimlaneSpaceBefore>
</elementInfo>
' #txt
>Proto Ps0 0 0 32 24 18 0 #rect
>Proto Ps0 @|BIcon #fIcon
Ps0 f0 mainOut f4 tail #connect
Ps0 f4 head f3 mainIn #connect
Ps0 f12 out f10 tail #connect
Ps0 f10 head f8 mainIn #connect
Ps0 f8 mainOut f17 tail #connect
Ps0 f17 head f15 in #connect
Ps0 f15 out f14 tail #connect
Ps0 f14 head f16 mainIn #connect
Ps0 f7 mainOut f6 tail #connect
Ps0 f6 head f1 mainIn #connect
Ps0 f12 out f20 tail #connect
Ps0 f20 head f19 mainIn #connect
Ps0 f19 mainOut f21 tail #connect
Ps0 f21 head f1 mainIn #connect
Ps0 f16 mainOut f22 tail #connect
Ps0 f22 head f11 in #connect
Ps0 f11 out f18 tail #connect
Ps0 f18 head f7 mainIn #connect
Ps0 f11 out f24 tail #connect
Ps0 f24 head f23 mainIn #connect
Ps0 f23 mainOut f25 tail #connect
Ps0 f25 head f1 mainIn #connect
Ps0 f3 mainOut f2 tail #connect
Ps0 f2 head f12 in #connect
