[Ivy]
1655FC922971693F 3.20 #module
>Proto >Proto Collection #zClass
Ps0 PermissionSettingProcess Big #zClass
Ps0 B #cInfo
Ps0 #process
Ps0 @TextInP .resExport .resExport #zField
Ps0 @TextInP .type .type #zField
Ps0 @TextInP .processKind .processKind #zField
Ps0 @AnnotationInP-0n ai ai #zField
Ps0 @MessageFlowInP-0n messageIn messageIn #zField
Ps0 @MessageFlowOutP-0n messageOut messageOut #zField
Ps0 @TextInP .xml .xml #zField
Ps0 @TextInP .responsibility .responsibility #zField
Ps0 @StartRequest f0 '' #zField
Ps0 @EndTask f1 '' #zField
Ps0 @GridStep f3 '' #zField
Ps0 @PushWFArc f4 '' #zField
Ps0 @PushWFArc f2 '' #zField
Ps0 @InfoButton f5 '' #zField
>Proto Ps0 Ps0 PermissionSettingProcess #zField
Ps0 f0 outLink start.ivp #txt
Ps0 f0 type ivy.advanced.exercise3.PermissionSettingProcessData #txt
Ps0 f0 inParamDecl '<> param;' #txt
Ps0 f0 actionDecl 'ivy.advanced.exercise3.PermissionSettingProcessData out;
' #txt
Ps0 f0 guid 1655FC922AC2F605 #txt
Ps0 f0 requestEnabled true #txt
Ps0 f0 triggerEnabled false #txt
Ps0 f0 callSignature start() #txt
Ps0 f0 caseData businessCase.attach=true #txt
Ps0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
Ps0 f0 @C|.responsibility Everybody #txt
Ps0 f0 81 49 30 30 -21 17 #rect
Ps0 f0 @|StartRequestIcon #fIcon
Ps0 f1 type ivy.advanced.exercise3.PermissionSettingProcessData #txt
Ps0 f1 369 49 30 30 0 15 #rect
Ps0 f1 @|EndIcon #fIcon
Ps0 f3 actionDecl 'ivy.advanced.exercise3.PermissionSettingProcessData out;
' #txt
Ps0 f3 actionTable 'out=in;
' #txt
Ps0 f3 actionCode 'import ch.ivyteam.ivy.security.IRole;
import ch.ivyteam.ivy.security.IPermission;

IPermission permission = IPermission.DOCUMENT_READ;

IRole superiorRole = ivy.wf.getSecurityContext().findRole("Superior");

ivy.wf.getApplication().getSecurityDescriptor().grantPermission(IPermission.TASK_READ_ALL, superiorRole);
ivy.wf.getApplication().getSecurityDescriptor().grantPermission(IPermission.TASK_READ_ALL_OWN_WORKED_ON, superiorRole);
ivy.wf.getApplication().getSecurityDescriptor().grantPermission(IPermission.ROLE_READ_ALL, superiorRole);
ivy.wf.getApplication().getSecurityDescriptor().grantPermission(IPermission.USER_READ_ROLES, superiorRole);
ivy.wf.getApplication().getSecurityDescriptor().grantPermission(IPermission.SYSTEM_PROPERTY_READ_ALL, superiorRole);

ivy.log.info("Set permission successfully!");' #txt
Ps0 f3 security system #txt
Ps0 f3 type ivy.advanced.exercise3.PermissionSettingProcessData #txt
Ps0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Set permission</name>
        <nameStyle>14,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f3 168 42 112 44 -42 -8 #rect
Ps0 f3 @|StepIcon #fIcon
Ps0 f4 expr out #txt
Ps0 f4 111 64 168 64 #arcP
Ps0 f2 expr out #txt
Ps0 f2 280 64 369 64 #arcP
Ps0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Remember to config on server when deploy</name>
        <nameStyle>40,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ps0 f5 96 97 256 30 -119 -8 #rect
Ps0 f5 @|IBIcon #fIcon
>Proto Ps0 .type ivy.advanced.exercise3.PermissionSettingProcessData #txt
>Proto Ps0 .processKind NORMAL #txt
>Proto Ps0 0 0 32 24 18 0 #rect
>Proto Ps0 @|BIcon #fIcon
Ps0 f0 mainOut f4 tail #connect
Ps0 f4 head f3 mainIn #connect
Ps0 f3 mainOut f2 tail #connect
Ps0 f2 head f1 mainIn #connect
