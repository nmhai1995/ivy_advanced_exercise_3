package ivy.advanced.exercise3.LoginDialog;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class LoginDialogData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class LoginDialogData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 3989312737033912096L;

  private ivy.advanced.exercise3.LoginInformation loginInformation;

  /**
   * Gets the field loginInformation.
   * @return the value of the field loginInformation; may be null.
   */
  public ivy.advanced.exercise3.LoginInformation getLoginInformation()
  {
    return loginInformation;
  }

  /**
   * Sets the field loginInformation.
   * @param _loginInformation the new value of the field loginInformation.
   */
  public void setLoginInformation(ivy.advanced.exercise3.LoginInformation _loginInformation)
  {
    loginInformation = _loginInformation;
  }

  private ivy.advanced.exercise3.UserInformation userInformation;

  /**
   * Gets the field userInformation.
   * @return the value of the field userInformation; may be null.
   */
  public ivy.advanced.exercise3.UserInformation getUserInformation()
  {
    return userInformation;
  }

  /**
   * Sets the field userInformation.
   * @param _userInformation the new value of the field userInformation.
   */
  public void setUserInformation(ivy.advanced.exercise3.UserInformation _userInformation)
  {
    userInformation = _userInformation;
  }

}
