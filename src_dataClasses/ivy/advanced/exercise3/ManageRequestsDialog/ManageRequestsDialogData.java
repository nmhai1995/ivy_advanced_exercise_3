package ivy.advanced.exercise3.ManageRequestsDialog;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ManageRequestsDialogData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ManageRequestsDialogData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 3763028954989933331L;

  private ivy.advanced.exercise3.LoginInformation loginInformation;

  /**
   * Gets the field loginInformation.
   * @return the value of the field loginInformation; may be null.
   */
  public ivy.advanced.exercise3.LoginInformation getLoginInformation()
  {
    return loginInformation;
  }

  /**
   * Sets the field loginInformation.
   * @param _loginInformation the new value of the field loginInformation.
   */
  public void setLoginInformation(ivy.advanced.exercise3.LoginInformation _loginInformation)
  {
    loginInformation = _loginInformation;
  }

  private ivy.advanced.exercise3.UserInformation userInformation;

  /**
   * Gets the field userInformation.
   * @return the value of the field userInformation; may be null.
   */
  public ivy.advanced.exercise3.UserInformation getUserInformation()
  {
    return userInformation;
  }

  /**
   * Sets the field userInformation.
   * @param _userInformation the new value of the field userInformation.
   */
  public void setUserInformation(ivy.advanced.exercise3.UserInformation _userInformation)
  {
    userInformation = _userInformation;
  }

  private java.lang.Boolean isThereAnyTask;

  /**
   * Gets the field isThereAnyTask.
   * @return the value of the field isThereAnyTask; may be null.
   */
  public java.lang.Boolean getIsThereAnyTask()
  {
    return isThereAnyTask;
  }

  /**
   * Sets the field isThereAnyTask.
   * @param _isThereAnyTask the new value of the field isThereAnyTask.
   */
  public void setIsThereAnyTask(java.lang.Boolean _isThereAnyTask)
  {
    isThereAnyTask = _isThereAnyTask;
  }

}
