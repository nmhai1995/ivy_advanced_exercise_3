package ivy.advanced.exercise3;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Reason", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Reason extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -6295422397656646289L;

  private java.lang.String workingHoliday;

  /**
   * Gets the field workingHoliday.
   * @return the value of the field workingHoliday; may be null.
   */
  public java.lang.String getWorkingHoliday()
  {
    return workingHoliday;
  }

  /**
   * Sets the field workingHoliday.
   * @param _workingHoliday the new value of the field workingHoliday.
   */
  public void setWorkingHoliday(java.lang.String _workingHoliday)
  {
    workingHoliday = _workingHoliday;
  }

  private java.lang.String mariage;

  /**
   * Gets the field mariage.
   * @return the value of the field mariage; may be null.
   */
  public java.lang.String getMariage()
  {
    return mariage;
  }

  /**
   * Sets the field mariage.
   * @param _mariage the new value of the field mariage.
   */
  public void setMariage(java.lang.String _mariage)
  {
    mariage = _mariage;
  }

  private java.lang.String sickness;

  /**
   * Gets the field sickness.
   * @return the value of the field sickness; may be null.
   */
  public java.lang.String getSickness()
  {
    return sickness;
  }

  /**
   * Sets the field sickness.
   * @param _sickness the new value of the field sickness.
   */
  public void setSickness(java.lang.String _sickness)
  {
    sickness = _sickness;
  }

  private java.lang.String maternityLeave;

  /**
   * Gets the field maternityLeave.
   * @return the value of the field maternityLeave; may be null.
   */
  public java.lang.String getMaternityLeave()
  {
    return maternityLeave;
  }

  /**
   * Sets the field maternityLeave.
   * @param _maternityLeave the new value of the field maternityLeave.
   */
  public void setMaternityLeave(java.lang.String _maternityLeave)
  {
    maternityLeave = _maternityLeave;
  }

  private java.lang.String unpair;

  /**
   * Gets the field unpair.
   * @return the value of the field unpair; may be null.
   */
  public java.lang.String getUnpair()
  {
    return unpair;
  }

  /**
   * Sets the field unpair.
   * @param _unpair the new value of the field unpair.
   */
  public void setUnpair(java.lang.String _unpair)
  {
    unpair = _unpair;
  }

  private java.lang.String other;

  /**
   * Gets the field other.
   * @return the value of the field other; may be null.
   */
  public java.lang.String getOther()
  {
    return other;
  }

  /**
   * Sets the field other.
   * @param _other the new value of the field other.
   */
  public void setOther(java.lang.String _other)
  {
    other = _other;
  }

}
