package ivy.advanced.exercise3;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class ProcessData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class ProcessData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -5525272052512159567L;

  private ivy.advanced.exercise3.LoginInformation loginInformation;

  /**
   * Gets the field loginInformation.
   * @return the value of the field loginInformation; may be null.
   */
  public ivy.advanced.exercise3.LoginInformation getLoginInformation()
  {
    return loginInformation;
  }

  /**
   * Sets the field loginInformation.
   * @param _loginInformation the new value of the field loginInformation.
   */
  public void setLoginInformation(ivy.advanced.exercise3.LoginInformation _loginInformation)
  {
    loginInformation = _loginInformation;
  }

  private ivy.advanced.exercise3.UserInformation userInformation;

  /**
   * Gets the field userInformation.
   * @return the value of the field userInformation; may be null.
   */
  public ivy.advanced.exercise3.UserInformation getUserInformation()
  {
    return userInformation;
  }

  /**
   * Sets the field userInformation.
   * @param _userInformation the new value of the field userInformation.
   */
  public void setUserInformation(ivy.advanced.exercise3.UserInformation _userInformation)
  {
    userInformation = _userInformation;
  }

  private ivy.advanced.exercise3.RequestInformation requestInformation;

  /**
   * Gets the field requestInformation.
   * @return the value of the field requestInformation; may be null.
   */
  public ivy.advanced.exercise3.RequestInformation getRequestInformation()
  {
    return requestInformation;
  }

  /**
   * Sets the field requestInformation.
   * @param _requestInformation the new value of the field requestInformation.
   */
  public void setRequestInformation(ivy.advanced.exercise3.RequestInformation _requestInformation)
  {
    requestInformation = _requestInformation;
  }

}
