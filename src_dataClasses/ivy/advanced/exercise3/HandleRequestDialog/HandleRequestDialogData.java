package ivy.advanced.exercise3.HandleRequestDialog;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class HandleRequestDialogData", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class HandleRequestDialogData extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -7694554743472931643L;

  private ivy.advanced.exercise3.UserInformation userInformation;

  /**
   * Gets the field userInformation.
   * @return the value of the field userInformation; may be null.
   */
  public ivy.advanced.exercise3.UserInformation getUserInformation()
  {
    return userInformation;
  }

  /**
   * Sets the field userInformation.
   * @param _userInformation the new value of the field userInformation.
   */
  public void setUserInformation(ivy.advanced.exercise3.UserInformation _userInformation)
  {
    userInformation = _userInformation;
  }

  private ivy.advanced.exercise3.RequestInformation requestInformation;

  /**
   * Gets the field requestInformation.
   * @return the value of the field requestInformation; may be null.
   */
  public ivy.advanced.exercise3.RequestInformation getRequestInformation()
  {
    return requestInformation;
  }

  /**
   * Sets the field requestInformation.
   * @param _requestInformation the new value of the field requestInformation.
   */
  public void setRequestInformation(ivy.advanced.exercise3.RequestInformation _requestInformation)
  {
    requestInformation = _requestInformation;
  }

  private java.util.List<java.lang.String> tasks;

  /**
   * Gets the field tasks.
   * @return the value of the field tasks; may be null.
   */
  public java.util.List<java.lang.String> getTasks()
  {
    return tasks;
  }

  /**
   * Sets the field tasks.
   * @param _tasks the new value of the field tasks.
   */
  public void setTasks(java.util.List<java.lang.String> _tasks)
  {
    tasks = _tasks;
  }

  private java.lang.String currentTaskName;

  /**
   * Gets the field currentTaskName.
   * @return the value of the field currentTaskName; may be null.
   */
  public java.lang.String getCurrentTaskName()
  {
    return currentTaskName;
  }

  /**
   * Sets the field currentTaskName.
   * @param _currentTaskName the new value of the field currentTaskName.
   */
  public void setCurrentTaskName(java.lang.String _currentTaskName)
  {
    currentTaskName = _currentTaskName;
  }

  private java.util.Map tasksMap;

  /**
   * Gets the field tasksMap.
   * @return the value of the field tasksMap; may be null.
   */
  public java.util.Map getTasksMap()
  {
    return tasksMap;
  }

  /**
   * Sets the field tasksMap.
   * @param _tasksMap the new value of the field tasksMap.
   */
  public void setTasksMap(java.util.Map _tasksMap)
  {
    tasksMap = _tasksMap;
  }

  private java.lang.String targetTaskName;

  /**
   * Gets the field targetTaskName.
   * @return the value of the field targetTaskName; may be null.
   */
  public java.lang.String getTargetTaskName()
  {
    return targetTaskName;
  }

  /**
   * Sets the field targetTaskName.
   * @param _targetTaskName the new value of the field targetTaskName.
   */
  public void setTargetTaskName(java.lang.String _targetTaskName)
  {
    targetTaskName = _targetTaskName;
  }

  private java.lang.String targetTaskPath;

  /**
   * Gets the field targetTaskPath.
   * @return the value of the field targetTaskPath; may be null.
   */
  public java.lang.String getTargetTaskPath()
  {
    return targetTaskPath;
  }

  /**
   * Sets the field targetTaskPath.
   * @param _targetTaskPath the new value of the field targetTaskPath.
   */
  public void setTargetTaskPath(java.lang.String _targetTaskPath)
  {
    targetTaskPath = _targetTaskPath;
  }

}
