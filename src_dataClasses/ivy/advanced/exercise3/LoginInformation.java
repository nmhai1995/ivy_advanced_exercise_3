package ivy.advanced.exercise3;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class LoginInformation", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class LoginInformation extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 3166237841257828542L;

  private java.lang.String username;

  /**
   * Gets the field username.
   * @return the value of the field username; may be null.
   */
  public java.lang.String getUsername()
  {
    return username;
  }

  /**
   * Sets the field username.
   * @param _username the new value of the field username.
   */
  public void setUsername(java.lang.String _username)
  {
    username = _username;
  }

  private java.lang.String password;

  /**
   * Gets the field password.
   * @return the value of the field password; may be null.
   */
  public java.lang.String getPassword()
  {
    return password;
  }

  /**
   * Sets the field password.
   * @param _password the new value of the field password.
   */
  public void setPassword(java.lang.String _password)
  {
    password = _password;
  }

  private java.lang.Boolean isAuthenticated;

  /**
   * Gets the field isAuthenticated.
   * @return the value of the field isAuthenticated; may be null.
   */
  public java.lang.Boolean getIsAuthenticated()
  {
    return isAuthenticated;
  }

  /**
   * Sets the field isAuthenticated.
   * @param _isAuthenticated the new value of the field isAuthenticated.
   */
  public void setIsAuthenticated(java.lang.Boolean _isAuthenticated)
  {
    isAuthenticated = _isAuthenticated;
  }

}
