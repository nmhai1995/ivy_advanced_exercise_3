package ivy.advanced.exercise3;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class RequestInformation", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class RequestInformation extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 8904430577507348864L;

  private java.lang.String fullName;

  /**
   * Gets the field fullName.
   * @return the value of the field fullName; may be null.
   */
  public java.lang.String getFullName()
  {
    return fullName;
  }

  /**
   * Sets the field fullName.
   * @param _fullName the new value of the field fullName.
   */
  public void setFullName(java.lang.String _fullName)
  {
    fullName = _fullName;
  }

  private java.lang.String email;

  /**
   * Gets the field email.
   * @return the value of the field email; may be null.
   */
  public java.lang.String getEmail()
  {
    return email;
  }

  /**
   * Sets the field email.
   * @param _email the new value of the field email.
   */
  public void setEmail(java.lang.String _email)
  {
    email = _email;
  }

  private java.util.Date startDate;

  /**
   * Gets the field startDate.
   * @return the value of the field startDate; may be null.
   */
  public java.util.Date getStartDate()
  {
    return startDate;
  }

  /**
   * Sets the field startDate.
   * @param _startDate the new value of the field startDate.
   */
  public void setStartDate(java.util.Date _startDate)
  {
    startDate = _startDate;
  }

  private java.util.Date endDate;

  /**
   * Gets the field endDate.
   * @return the value of the field endDate; may be null.
   */
  public java.util.Date getEndDate()
  {
    return endDate;
  }

  /**
   * Sets the field endDate.
   * @param _endDate the new value of the field endDate.
   */
  public void setEndDate(java.util.Date _endDate)
  {
    endDate = _endDate;
  }

  private java.lang.String type;

  /**
   * Gets the field type.
   * @return the value of the field type; may be null.
   */
  public java.lang.String getType()
  {
    return type;
  }

  /**
   * Sets the field type.
   * @param _type the new value of the field type.
   */
  public void setType(java.lang.String _type)
  {
    type = _type;
  }

  private java.lang.Boolean isApproved;

  /**
   * Gets the field isApproved.
   * @return the value of the field isApproved; may be null.
   */
  public java.lang.Boolean getIsApproved()
  {
    return isApproved;
  }

  /**
   * Sets the field isApproved.
   * @param _isApproved the new value of the field isApproved.
   */
  public void setIsApproved(java.lang.Boolean _isApproved)
  {
    isApproved = _isApproved;
  }

}
