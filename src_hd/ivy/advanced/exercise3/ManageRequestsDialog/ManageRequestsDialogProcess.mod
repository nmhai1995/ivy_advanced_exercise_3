[Ivy]
1655AA44B79646EC 3.20 #module
>Proto >Proto Collection #zClass
Ms0 ManageRequestsDialogProcess Big #zClass
Ms0 RD #cInfo
Ms0 #process
Ms0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Ms0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Ms0 @TextInP .resExport .resExport #zField
Ms0 @TextInP .type .type #zField
Ms0 @TextInP .processKind .processKind #zField
Ms0 @AnnotationInP-0n ai ai #zField
Ms0 @MessageFlowInP-0n messageIn messageIn #zField
Ms0 @MessageFlowOutP-0n messageOut messageOut #zField
Ms0 @TextInP .xml .xml #zField
Ms0 @TextInP .responsibility .responsibility #zField
Ms0 @RichDialogInitStart f0 '' #zField
Ms0 @RichDialogProcessEnd f1 '' #zField
Ms0 @RichDialogProcessStart f3 '' #zField
Ms0 @RichDialogEnd f4 '' #zField
Ms0 @PushWFArc f5 '' #zField
Ms0 @GridStep f16 '' #zField
Ms0 @GridStep f25 '' #zField
Ms0 @RichDialogProcessStart f17 '' #zField
Ms0 @RichDialogProcessEnd f18 '' #zField
Ms0 @PushWFArc f26 '' #zField
Ms0 @PushWFArc f19 '' #zField
Ms0 @PushWFArc f20 '' #zField
Ms0 @GridStep f8 '' #zField
Ms0 @Alternative f14 '' #zField
Ms0 @GridStep f15 '' #zField
Ms0 @PushWFArc f21 '' #zField
Ms0 @PushWFArc f22 '' #zField
Ms0 @RichDialogProcessStart f12 '' #zField
Ms0 @PushWFArc f13 '' #zField
Ms0 @RichDialogProcessEnd f23 '' #zField
Ms0 @PushWFArc f24 '' #zField
Ms0 @GridStep f7 '' #zField
Ms0 @PushWFArc f9 '' #zField
Ms0 @PushWFArc f2 '' #zField
Ms0 @GridStep f10 '' #zField
Ms0 @PushWFArc f11 '' #zField
Ms0 @PushWFArc f6 '' #zField
>Proto Ms0 Ms0 ManageRequestsDialogProcess #zField
Ms0 f0 guid 1655AA44B8521B05 #txt
Ms0 f0 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f0 method start(ivy.advanced.exercise3.LoginInformation,ivy.advanced.exercise3.UserInformation) #txt
Ms0 f0 disableUIEvents true #txt
Ms0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<ivy.advanced.exercise3.LoginInformation loginInformation,ivy.advanced.exercise3.UserInformation userInformation> param = methodEvent.getInputArguments();
' #txt
Ms0 f0 inParameterMapAction 'out.loginInformation=param.loginInformation;
out.userInformation=param.userInformation;
' #txt
Ms0 f0 outParameterDecl '<> result;
' #txt
Ms0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f0 83 51 26 26 -16 15 #rect
Ms0 f0 @|RichDialogInitStartIcon #fIcon
Ms0 f1 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f1 339 51 26 26 0 12 #rect
Ms0 f1 @|RichDialogProcessEndIcon #fIcon
Ms0 f3 guid 1655AA44B8FDC9C3 #txt
Ms0 f3 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f3 actionDecl 'ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData out;
' #txt
Ms0 f3 actionTable 'out=in;
' #txt
Ms0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Ms0 f3 83 211 26 26 -15 12 #rect
Ms0 f3 @|RichDialogProcessStartIcon #fIcon
Ms0 f4 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f4 guid 1655AA44B8F979F0 #txt
Ms0 f4 211 211 26 26 0 12 #rect
Ms0 f4 @|RichDialogEndIcon #fIcon
Ms0 f5 expr out #txt
Ms0 f5 109 224 211 224 #arcP
Ms0 f16 actionDecl 'ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData out;
' #txt
Ms0 f16 actionTable 'out=in;
' #txt
Ms0 f16 actionCode 'ivy.session.logoutSessionUser();' #txt
Ms0 f16 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Logout session</name>
        <nameStyle>14,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f16 166 298 112 44 -43 -8 #rect
Ms0 f16 @|StepIcon #fIcon
Ms0 f25 actionDecl 'ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData out;
' #txt
Ms0 f25 actionTable 'out=in;
' #txt
Ms0 f25 actionCode 'import javax.faces.context.FacesContext;

FacesContext.getCurrentInstance().getExternalContext().redirect(ivy.html.startref("Start Processes/Process/start.ivp"));' #txt
Ms0 f25 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Redirect to login page</name>
        <nameStyle>22,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f25 329 299 128 44 -60 -8 #rect
Ms0 f25 @|StepIcon #fIcon
Ms0 f17 guid 16564B7BCDCE151D #txt
Ms0 f17 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f17 actionDecl 'ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData out;
' #txt
Ms0 f17 actionTable 'out=in;
' #txt
Ms0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>logout</name>
        <nameStyle>6,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f17 84 307 26 26 -17 15 #rect
Ms0 f17 @|RichDialogProcessStartIcon #fIcon
Ms0 f18 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f18 500 308 26 26 0 12 #rect
Ms0 f18 @|RichDialogProcessEndIcon #fIcon
Ms0 f26 expr out #txt
Ms0 f26 278 320 329 321 #arcP
Ms0 f19 expr out #txt
Ms0 f19 110 320 166 320 #arcP
Ms0 f20 expr out #txt
Ms0 f20 457 321 500 321 #arcP
Ms0 f8 actionDecl 'ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData out;
' #txt
Ms0 f8 actionTable 'out=in;
' #txt
Ms0 f8 actionCode 'import ch.ivyteam.ivy.workflow.query.TaskQuery;
import ch.ivyteam.ivy.security.IUser;
import ch.ivyteam.ivy.workflow.ITask;
import javax.faces.context.FacesContext;

IUser user = ivy.session.getSessionUser();
TaskQuery query = TaskQuery.create().where().canWorkOn(user);

ITask task = ivy.wf.getTaskQueryExecutor().getResults(query).get(0) as ITask;
FacesContext.getCurrentInstance().getExternalContext().redirect( ivy.html.taskstartref(task));' #txt
Ms0 f8 security system #txt
Ms0 f8 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Go to the first task in taskList</name>
        <nameStyle>32,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f8 421 394 176 44 -78 -8 #rect
Ms0 f8 @|StepIcon #fIcon
Ms0 f14 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Is there any task?</name>
        <nameStyle>18,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f14 325 400 32 32 -48 -36 #rect
Ms0 f14 @|AlternativeIcon #fIcon
Ms0 f15 actionDecl 'ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData out;
' #txt
Ms0 f15 actionTable 'out=in;
' #txt
Ms0 f15 actionCode 'import ch.ivyteam.ivy.workflow.query.TaskQuery;
import ch.ivyteam.ivy.security.IUser;

IUser user = ivy.session.getSessionUser();
TaskQuery query = TaskQuery.create().where().canWorkOn(user);

if (ivy.wf.getTaskQueryExecutor().getResults(query).isEmpty()) {
	in.isThereAnyTask = false;	
} else {
	in.isThereAnyTask = true;	
}' #txt
Ms0 f15 security system #txt
Ms0 f15 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get all tasks</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f15 157 394 112 44 -34 -8 #rect
Ms0 f15 @|StepIcon #fIcon
Ms0 f21 expr out #txt
Ms0 f21 269 416 325 416 #arcP
Ms0 f22 expr in #txt
Ms0 f22 outCond in.isThereAnyTask #txt
Ms0 f22 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Yes</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f22 357 416 421 416 #arcP
Ms0 f22 0 0.4375 1 -9 #arcLabel
Ms0 f12 guid 16564F4C01369E02 #txt
Ms0 f12 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f12 actionDecl 'ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData out;
' #txt
Ms0 f12 actionTable 'out=in;
' #txt
Ms0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>handleFlow</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f12 83 403 26 26 -32 15 #rect
Ms0 f12 @|RichDialogProcessStartIcon #fIcon
Ms0 f13 expr out #txt
Ms0 f13 109 416 157 416 #arcP
Ms0 f23 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f23 659 403 26 26 0 12 #rect
Ms0 f23 @|RichDialogProcessEndIcon #fIcon
Ms0 f24 expr out #txt
Ms0 f24 597 416 659 416 #arcP
Ms0 f7 actionDecl 'ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData out;
' #txt
Ms0 f7 actionTable 'out=in;
' #txt
Ms0 f7 actionCode 'in.isThereAnyTask = true;' #txt
Ms0 f7 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Initiallize</name>
        <nameStyle>11,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f7 168 42 112 44 -23 -8 #rect
Ms0 f7 @|StepIcon #fIcon
Ms0 f9 expr out #txt
Ms0 f9 109 64 168 64 #arcP
Ms0 f2 expr out #txt
Ms0 f2 280 64 339 64 #arcP
Ms0 f10 actionDecl 'ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData out;
' #txt
Ms0 f10 actionTable 'out=in;
' #txt
Ms0 f10 actionCode 'import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

String noTaskMessage = "There is no task waiting for you!";
FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, noTaskMessage, ""));' #txt
Ms0 f10 type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
Ms0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show message</name>
        <nameStyle>12,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f10 448 458 112 44 -43 -8 #rect
Ms0 f10 @|StepIcon #fIcon
Ms0 f11 expr in #txt
Ms0 f11 outCond !in.isThereAnyTask #txt
Ms0 f11 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>No</name>
        <nameStyle>2,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f11 350 423 448 480 #arcP
Ms0 f11 1 416 480 #addKink
Ms0 f11 0 0.5191321499013807 -7 9 #arcLabel
Ms0 f6 expr out #txt
Ms0 f6 560 480 672 429 #arcP
Ms0 f6 1 672 480 #addKink
Ms0 f6 0 0.5191321499013807 -7 9 #arcLabel
>Proto Ms0 .type ivy.advanced.exercise3.ManageRequestsDialog.ManageRequestsDialogData #txt
>Proto Ms0 .processKind HTML_DIALOG #txt
>Proto Ms0 -8 -8 16 16 16 26 #rect
>Proto Ms0 '' #fIcon
Ms0 f3 mainOut f5 tail #connect
Ms0 f5 head f4 mainIn #connect
Ms0 f17 mainOut f19 tail #connect
Ms0 f19 head f16 mainIn #connect
Ms0 f16 mainOut f26 tail #connect
Ms0 f26 head f25 mainIn #connect
Ms0 f25 mainOut f20 tail #connect
Ms0 f20 head f18 mainIn #connect
Ms0 f15 mainOut f21 tail #connect
Ms0 f21 head f14 in #connect
Ms0 f14 out f22 tail #connect
Ms0 f22 head f8 mainIn #connect
Ms0 f12 mainOut f13 tail #connect
Ms0 f13 head f15 mainIn #connect
Ms0 f8 mainOut f24 tail #connect
Ms0 f24 head f23 mainIn #connect
Ms0 f0 mainOut f9 tail #connect
Ms0 f9 head f7 mainIn #connect
Ms0 f7 mainOut f2 tail #connect
Ms0 f2 head f1 mainIn #connect
Ms0 f14 out f11 tail #connect
Ms0 f11 head f10 mainIn #connect
Ms0 f10 mainOut f6 tail #connect
Ms0 f6 head f23 mainIn #connect
