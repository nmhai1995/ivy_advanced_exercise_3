[Ivy]
16556F86FC213860 3.20 #module
>Proto >Proto Collection #zClass
Ls0 LeavingRequestDialogProcess Big #zClass
Ls0 RD #cInfo
Ls0 #process
Ls0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Ls0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Ls0 @TextInP .resExport .resExport #zField
Ls0 @TextInP .type .type #zField
Ls0 @TextInP .processKind .processKind #zField
Ls0 @AnnotationInP-0n ai ai #zField
Ls0 @MessageFlowInP-0n messageIn messageIn #zField
Ls0 @MessageFlowOutP-0n messageOut messageOut #zField
Ls0 @TextInP .xml .xml #zField
Ls0 @TextInP .responsibility .responsibility #zField
Ls0 @RichDialogInitStart f0 '' #zField
Ls0 @RichDialogProcessEnd f1 '' #zField
Ls0 @RichDialogProcessStart f3 '' #zField
Ls0 @RichDialogEnd f4 '' #zField
Ls0 @PushWFArc f5 '' #zField
Ls0 @GridStep f6 '' #zField
Ls0 @PushWFArc f7 '' #zField
Ls0 @PushWFArc f2 '' #zField
Ls0 @RichDialogProcessStart f13 '' #zField
Ls0 @GridStep f14 '' #zField
Ls0 @PushWFArc f16 '' #zField
Ls0 @RichDialogEnd f15 '' #zField
Ls0 @PushWFArc f17 '' #zField
Ls0 @GridStep f8 '' #zField
Ls0 @GridStep f25 '' #zField
Ls0 @RichDialogProcessStart f9 '' #zField
Ls0 @RichDialogProcessEnd f18 '' #zField
Ls0 @PushWFArc f26 '' #zField
Ls0 @PushWFArc f19 '' #zField
Ls0 @PushWFArc f20 '' #zField
>Proto Ls0 Ls0 LeavingRequestDialogProcess #zField
Ls0 f0 guid 16556F86FFCDF027 #txt
Ls0 f0 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f0 method start(ivy.advanced.exercise3.UserInformation) #txt
Ls0 f0 disableUIEvents true #txt
Ls0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<ivy.advanced.exercise3.UserInformation userInformation> param = methodEvent.getInputArguments();
' #txt
Ls0 f0 inParameterMapAction 'out.userInformation=param.userInformation;
' #txt
Ls0 f0 outParameterDecl '<ivy.advanced.exercise3.RequestInformation requestInformation> result;
' #txt
Ls0 f0 outParameterMapAction 'result.requestInformation=in.requestInformation;
' #txt
Ls0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f0 83 51 26 26 -16 15 #rect
Ls0 f0 @|RichDialogInitStartIcon #fIcon
Ls0 f1 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f1 403 51 26 26 0 12 #rect
Ls0 f1 @|RichDialogProcessEndIcon #fIcon
Ls0 f3 guid 16556F8701592861 #txt
Ls0 f3 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f3 actionDecl 'ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData out;
' #txt
Ls0 f3 actionTable 'out=in;
' #txt
Ls0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Ls0 f3 83 147 26 26 -15 12 #rect
Ls0 f3 @|RichDialogProcessStartIcon #fIcon
Ls0 f4 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f4 guid 16556F8701558CB5 #txt
Ls0 f4 211 147 26 26 0 12 #rect
Ls0 f4 @|RichDialogEndIcon #fIcon
Ls0 f5 expr out #txt
Ls0 f5 109 160 211 160 #arcP
Ls0 f6 actionDecl 'ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData out;
' #txt
Ls0 f6 actionTable 'out=in;
' #txt
Ls0 f6 actionCode 'import ivy.advanced.exercise3.RequestInformation;
in.requestInformation = new RequestInformation();
in.requestInformation.fullName = in.userInformation.fullName;
in.requestInformation.email = in.userInformation.email;' #txt
Ls0 f6 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Initiallize RequestInformation</name>
        <nameStyle>30,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f6 168 42 176 44 -79 -8 #rect
Ls0 f6 @|StepIcon #fIcon
Ls0 f7 expr out #txt
Ls0 f7 109 64 168 64 #arcP
Ls0 f2 expr out #txt
Ls0 f2 344 64 403 64 #arcP
Ls0 f13 guid 1655BAF7C544C7C0 #txt
Ls0 f13 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f13 actionDecl 'ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData out;
' #txt
Ls0 f13 actionTable 'out=in;
' #txt
Ls0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>sendInformation</name>
        <nameStyle>15,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f13 83 243 26 26 -45 15 #rect
Ls0 f13 @|RichDialogProcessStartIcon #fIcon
Ls0 f14 actionDecl 'ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData out;
' #txt
Ls0 f14 actionTable 'out=in;
' #txt
Ls0 f14 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Send Emloyee Information </name>
    </language>
</elementInfo>
' #txt
Ls0 f14 176 234 160 44 -74 -8 #rect
Ls0 f14 @|StepIcon #fIcon
Ls0 f16 expr out #txt
Ls0 f16 109 256 176 256 #arcP
Ls0 f15 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f15 guid 1655BB35F0ADAC46 #txt
Ls0 f15 467 243 26 26 0 12 #rect
Ls0 f15 @|RichDialogEndIcon #fIcon
Ls0 f17 expr out #txt
Ls0 f17 336 256 467 256 #arcP
Ls0 f8 actionDecl 'ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData out;
' #txt
Ls0 f8 actionTable 'out=in;
' #txt
Ls0 f8 actionCode 'ivy.session.logoutSessionUser();' #txt
Ls0 f8 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Logout session</name>
        <nameStyle>14,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f8 161 329 112 44 -43 -8 #rect
Ls0 f8 @|StepIcon #fIcon
Ls0 f25 actionDecl 'ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData out;
' #txt
Ls0 f25 actionTable 'out=in;
' #txt
Ls0 f25 actionCode 'import javax.faces.context.FacesContext;

FacesContext.getCurrentInstance().getExternalContext().redirect(ivy.html.startref("Start Processes/Process/start.ivp"));' #txt
Ls0 f25 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Redirect to login page</name>
        <nameStyle>22,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f25 324 330 128 44 -60 -8 #rect
Ls0 f25 @|StepIcon #fIcon
Ls0 f9 guid 16564B8026513308 #txt
Ls0 f9 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f9 actionDecl 'ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData out;
' #txt
Ls0 f9 actionTable 'out=in;
' #txt
Ls0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>logout</name>
        <nameStyle>6,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f9 79 338 26 26 -17 15 #rect
Ls0 f9 @|RichDialogProcessStartIcon #fIcon
Ls0 f18 type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
Ls0 f18 495 339 26 26 0 12 #rect
Ls0 f18 @|RichDialogProcessEndIcon #fIcon
Ls0 f26 expr out #txt
Ls0 f26 273 351 324 352 #arcP
Ls0 f19 expr out #txt
Ls0 f19 105 351 161 351 #arcP
Ls0 f20 expr out #txt
Ls0 f20 452 352 495 352 #arcP
>Proto Ls0 .type ivy.advanced.exercise3.LeavingRequestDialog.LeavingRequestDialogData #txt
>Proto Ls0 .processKind HTML_DIALOG #txt
>Proto Ls0 -8 -8 16 16 16 26 #rect
>Proto Ls0 '' #fIcon
Ls0 f3 mainOut f5 tail #connect
Ls0 f5 head f4 mainIn #connect
Ls0 f0 mainOut f7 tail #connect
Ls0 f7 head f6 mainIn #connect
Ls0 f6 mainOut f2 tail #connect
Ls0 f2 head f1 mainIn #connect
Ls0 f13 mainOut f16 tail #connect
Ls0 f16 head f14 mainIn #connect
Ls0 f14 mainOut f17 tail #connect
Ls0 f17 head f15 mainIn #connect
Ls0 f9 mainOut f19 tail #connect
Ls0 f19 head f8 mainIn #connect
Ls0 f8 mainOut f26 tail #connect
Ls0 f26 head f25 mainIn #connect
Ls0 f25 mainOut f20 tail #connect
Ls0 f20 head f18 mainIn #connect
