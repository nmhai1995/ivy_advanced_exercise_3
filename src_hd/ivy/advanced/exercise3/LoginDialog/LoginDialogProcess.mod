[Ivy]
16546A43CFDF407F 3.20 #module
>Proto >Proto Collection #zClass
Ls0 LoginDialogProcess Big #zClass
Ls0 RD #cInfo
Ls0 #process
Ls0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Ls0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Ls0 @TextInP .resExport .resExport #zField
Ls0 @TextInP .type .type #zField
Ls0 @TextInP .processKind .processKind #zField
Ls0 @AnnotationInP-0n ai ai #zField
Ls0 @MessageFlowInP-0n messageIn messageIn #zField
Ls0 @MessageFlowOutP-0n messageOut messageOut #zField
Ls0 @TextInP .xml .xml #zField
Ls0 @TextInP .responsibility .responsibility #zField
Ls0 @RichDialogInitStart f0 '' #zField
Ls0 @RichDialogProcessEnd f1 '' #zField
Ls0 @RichDialogProcessStart f3 '' #zField
Ls0 @RichDialogEnd f4 '' #zField
Ls0 @GridStep f6 '' #zField
Ls0 @PushWFArc f2 '' #zField
Ls0 @GridStep f8 '' #zField
Ls0 @PushWFArc f9 '' #zField
Ls0 @Alternative f10 '' #zField
Ls0 @PushWFArc f11 '' #zField
Ls0 @GridStep f12 '' #zField
Ls0 @PushWFArc f13 '' #zField
Ls0 @GridStep f19 '' #zField
Ls0 @PushWFArc f17 '' #zField
Ls0 @RichDialogProcessEnd f18 '' #zField
Ls0 @PushWFArc f20 '' #zField
Ls0 @PushWFArc f7 '' #zField
Ls0 @Alternative f14 '' #zField
Ls0 @PushWFArc f15 '' #zField
Ls0 @PushWFArc f5 '' #zField
Ls0 @PushWFArc f16 '' #zField
>Proto Ls0 Ls0 LoginDialogProcess #zField
Ls0 f0 guid 16546A43D1D53ABC #txt
Ls0 f0 type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
Ls0 f0 method start() #txt
Ls0 f0 disableUIEvents true #txt
Ls0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Ls0 f0 outParameterDecl '<ivy.advanced.exercise3.LoginInformation loginInformation,ivy.advanced.exercise3.UserInformation userInformation> result;
' #txt
Ls0 f0 outParameterMapAction 'result.loginInformation=in.loginInformation;
result.userInformation=in.userInformation;
' #txt
Ls0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f0 83 51 26 26 -16 15 #rect
Ls0 f0 @|RichDialogInitStartIcon #fIcon
Ls0 f1 type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
Ls0 f1 499 51 26 26 0 12 #rect
Ls0 f1 @|RichDialogProcessEndIcon #fIcon
Ls0 f3 guid 16546A43D3C7C197 #txt
Ls0 f3 type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
Ls0 f3 actionDecl 'ivy.advanced.exercise3.LoginDialog.LoginDialogData out;
' #txt
Ls0 f3 actionTable 'out=in;
' #txt
Ls0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Ls0 f3 83 147 26 26 -15 12 #rect
Ls0 f3 @|RichDialogProcessStartIcon #fIcon
Ls0 f4 type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
Ls0 f4 guid 16546A43D3C40B7A #txt
Ls0 f4 787 147 26 26 0 12 #rect
Ls0 f4 @|RichDialogEndIcon #fIcon
Ls0 f6 actionDecl 'ivy.advanced.exercise3.LoginDialog.LoginDialogData out;
' #txt
Ls0 f6 actionTable 'out=in;
' #txt
Ls0 f6 actionCode 'import ivy.advanced.exercise3.UserInformation;
import ivy.advanced.exercise3.LoginInformation;

in.loginInformation = new LoginInformation();
in.userInformation = new UserInformation();
' #txt
Ls0 f6 type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
Ls0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Initiallize LoginInformation and UserInformation</name>
        <nameStyle>48,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f6 168 40 272 48 -129 -8 #rect
Ls0 f6 @|StepIcon #fIcon
Ls0 f2 expr out #txt
Ls0 f2 440 64 499 64 #arcP
Ls0 f2 0 0.4999999999999999 0 0 #arcLabel
Ls0 f8 actionDecl 'ivy.advanced.exercise3.LoginDialog.LoginDialogData out;
' #txt
Ls0 f8 actionTable 'out=in;
' #txt
Ls0 f8 actionCode 'import main.com.axonactive.exercise.login.LoginHandler;

in.loginInformation.isAuthenticated = false;
in.loginInformation.isAuthenticated = ivy.session.loginSessionUser(in.loginInformation.username, in.loginInformation.password);
ivy.log.info("User :" +in.loginInformation.username);
ivy.log.info("Pass :" +in.loginInformation.password);' #txt
Ls0 f8 type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
Ls0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Check login</name>
        <nameStyle>11,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f8 168 138 112 44 -32 -8 #rect
Ls0 f8 @|StepIcon #fIcon
Ls0 f9 expr out #txt
Ls0 f9 109 160 168 160 #arcP
Ls0 f10 type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
Ls0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Login successfully?</name>
        <nameStyle>19,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f10 336 144 32 32 -58 -40 #rect
Ls0 f10 @|AlternativeIcon #fIcon
Ls0 f11 expr out #txt
Ls0 f11 280 160 336 160 #arcP
Ls0 f12 actionDecl 'ivy.advanced.exercise3.LoginDialog.LoginDialogData out;
' #txt
Ls0 f12 actionTable 'out=in;
' #txt
Ls0 f12 actionCode 'import ch.ivyteam.ivy.security.IRole;
if (in.loginInformation.isAuthenticated == true)  {
	in.userInformation.fullName = ivy.session.getSessionUser().getFullName();
	in.userInformation.email = ivy.session.getSessionUser().getEMailAddress();
}

if (!ivy.session.getSessionUser().getAllRoles().isEmpty()) {
	for (IRole role : ivy.session.getSessionUser().getAllRoles()) {
		if (role.getName().equalsIgnoreCase("Superior")) {
			in.userInformation.role = role.getName();
			break;
		} else if (role.getName().equalsIgnoreCase("Employee")) {
			in.userInformation.role = role.getName();
			break;
		} else {
			in.userInformation.role = "";
		}	
	}
} else {
	in.userInformation.role = "";
}' #txt
Ls0 f12 type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
Ls0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get user''s information</name>
        <nameStyle>22,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f12 448 138 128 44 -60 -8 #rect
Ls0 f12 @|StepIcon #fIcon
Ls0 f13 expr in #txt
Ls0 f13 outCond in.loginInformation.isAuthenticated #txt
Ls0 f13 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Yes</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f13 368 160 448 160 #arcP
Ls0 f13 0 0.4583333333333333 0 -12 #arcLabel
Ls0 f19 actionDecl 'ivy.advanced.exercise3.LoginDialog.LoginDialogData out;
' #txt
Ls0 f19 actionTable 'out=in;
' #txt
Ls0 f19 actionCode 'import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

String invalidErrorMessage = "Invalid Username and Password.";
String permissionErrorMessage = "Permission access denied.";

if (FacesContext.getCurrentInstance() != null) {
	if (in.loginInformation.isAuthenticated == false) {
		FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, invalidErrorMessage, ""));
	} else if (in.userInformation.role != "Employee" && in.userInformation.role != "Superior") {
		FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, permissionErrorMessage, ""));
	}
}' #txt
Ls0 f19 type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
Ls0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show messages</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f19 585 234 112 44 -47 -8 #rect
Ls0 f19 @|StepIcon #fIcon
Ls0 f17 expr in #txt
Ls0 f17 outCond !in.loginInformation.isAuthenticated #txt
Ls0 f17 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>No</name>
        <nameStyle>2,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f17 352 176 585 256 #arcP
Ls0 f17 1 352 256 #addKink
Ls0 f17 1 0.29314288962631296 0 -11 #arcLabel
Ls0 f18 type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
Ls0 f18 787 243 26 26 0 12 #rect
Ls0 f18 @|RichDialogProcessEndIcon #fIcon
Ls0 f20 expr out #txt
Ls0 f20 697 256 787 256 #arcP
Ls0 f7 expr out #txt
Ls0 f7 109 64 168 64 #arcP
Ls0 f14 type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
Ls0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Check role</name>
        <nameStyle>10,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f14 624 144 32 32 -28 -33 #rect
Ls0 f14 @|AlternativeIcon #fIcon
Ls0 f15 expr out #txt
Ls0 f15 576 160 624 160 #arcP
Ls0 f5 expr in #txt
Ls0 f5 outCond 'in.userInformation.role == "Employee" || in.userInformation.role == "Superior"' #txt
Ls0 f5 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Valid</name>
        <nameStyle>5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f5 656 160 787 160 #arcP
Ls0 f5 0 0.5074626865671642 0 -10 #arcLabel
Ls0 f16 expr in #txt
Ls0 f16 outCond 'in.userInformation.role != "Employee" && in.userInformation.role != "Superior"' #txt
Ls0 f16 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Invalid</name>
        <nameStyle>7,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ls0 f16 640 176 641 234 #arcP
Ls0 f16 0 0.4716196136701337 21 0 #arcLabel
>Proto Ls0 .type ivy.advanced.exercise3.LoginDialog.LoginDialogData #txt
>Proto Ls0 .processKind HTML_DIALOG #txt
>Proto Ls0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>No</name>
        <nameStyle>2,7
</nameStyle>
    </language>
</elementInfo>
' #txt
>Proto Ls0 -8 -8 16 16 16 26 #rect
>Proto Ls0 '' #fIcon
Ls0 f6 mainOut f2 tail #connect
Ls0 f2 head f1 mainIn #connect
Ls0 f3 mainOut f9 tail #connect
Ls0 f9 head f8 mainIn #connect
Ls0 f8 mainOut f11 tail #connect
Ls0 f11 head f10 in #connect
Ls0 f10 out f13 tail #connect
Ls0 f13 head f12 mainIn #connect
Ls0 f10 out f17 tail #connect
Ls0 f17 head f19 mainIn #connect
Ls0 f19 mainOut f20 tail #connect
Ls0 f20 head f18 mainIn #connect
Ls0 f0 mainOut f7 tail #connect
Ls0 f7 head f6 mainIn #connect
Ls0 f12 mainOut f15 tail #connect
Ls0 f15 head f14 in #connect
Ls0 f14 out f5 tail #connect
Ls0 f5 head f4 mainIn #connect
Ls0 f14 out f16 tail #connect
Ls0 f16 head f19 mainIn #connect
