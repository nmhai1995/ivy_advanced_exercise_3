[Ivy]
165470AF3ABDEEA8 3.20 #module
>Proto >Proto Collection #zClass
hs0 HandleRequestDialogProcess Big #zClass
hs0 RD #cInfo
hs0 #process
hs0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
hs0 @TextInP .rdData2UIAction .rdData2UIAction #zField
hs0 @TextInP .resExport .resExport #zField
hs0 @TextInP .type .type #zField
hs0 @TextInP .processKind .processKind #zField
hs0 @AnnotationInP-0n ai ai #zField
hs0 @MessageFlowInP-0n messageIn messageIn #zField
hs0 @MessageFlowOutP-0n messageOut messageOut #zField
hs0 @TextInP .xml .xml #zField
hs0 @TextInP .responsibility .responsibility #zField
hs0 @RichDialogInitStart f0 '' #zField
hs0 @RichDialogProcessEnd f1 '' #zField
hs0 @RichDialogProcessStart f3 '' #zField
hs0 @RichDialogEnd f4 '' #zField
hs0 @PushWFArc f5 '' #zField
hs0 @RichDialogProcessStart f6 '' #zField
hs0 @RichDialogProcessStart f7 '' #zField
hs0 @RichDialogEnd f8 '' #zField
hs0 @RichDialogEnd f9 '' #zField
hs0 @GridStep f12 '' #zField
hs0 @PushWFArc f13 '' #zField
hs0 @PushWFArc f10 '' #zField
hs0 @GridStep f14 '' #zField
hs0 @PushWFArc f15 '' #zField
hs0 @PushWFArc f11 '' #zField
hs0 @GridStep f16 '' #zField
hs0 @RichDialogProcessStart f17 '' #zField
hs0 @RichDialogProcessEnd f18 '' #zField
hs0 @PushWFArc f19 '' #zField
hs0 @GridStep f21 '' #zField
hs0 @PushWFArc f22 '' #zField
hs0 @GridStep f23 '' #zField
hs0 @PushWFArc f24 '' #zField
hs0 @PushWFArc f2 '' #zField
hs0 @GridStep f25 '' #zField
hs0 @PushWFArc f26 '' #zField
hs0 @PushWFArc f20 '' #zField
hs0 @RichDialogMethodStart f27 '' #zField
hs0 @RichDialogProcessEnd f28 '' #zField
hs0 @GridStep f30 '' #zField
hs0 @PushWFArc f31 '' #zField
hs0 @PushWFArc f29 '' #zField
>Proto hs0 hs0 HandleRequestDialogProcess #zField
hs0 f0 guid 165470AF3CE79BE4 #txt
hs0 f0 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f0 method start(ivy.advanced.exercise3.RequestInformation,ivy.advanced.exercise3.UserInformation) #txt
hs0 f0 disableUIEvents true #txt
hs0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<ivy.advanced.exercise3.RequestInformation requestInformation,ivy.advanced.exercise3.UserInformation userInformation> param = methodEvent.getInputArguments();
' #txt
hs0 f0 inParameterMapAction 'out.requestInformation=param.requestInformation;
out.userInformation=param.userInformation;
' #txt
hs0 f0 outParameterDecl '<ivy.advanced.exercise3.RequestInformation requestInformation> result;
' #txt
hs0 f0 outParameterMapAction 'result.requestInformation=in.requestInformation;
' #txt
hs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f0 83 51 26 26 -16 15 #rect
hs0 f0 @|RichDialogInitStartIcon #fIcon
hs0 f1 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f1 659 51 26 26 0 12 #rect
hs0 f1 @|RichDialogProcessEndIcon #fIcon
hs0 f3 guid 165470AF3DB93BB0 #txt
hs0 f3 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f3 actionDecl 'ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData out;
' #txt
hs0 f3 actionTable 'out=in;
' #txt
hs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
hs0 f3 83 147 26 26 -15 12 #rect
hs0 f3 @|RichDialogProcessStartIcon #fIcon
hs0 f4 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f4 guid 165470AF3DB2AEDA #txt
hs0 f4 211 147 26 26 0 12 #rect
hs0 f4 @|RichDialogEndIcon #fIcon
hs0 f5 expr out #txt
hs0 f5 109 160 211 160 #arcP
hs0 f6 guid 1655A7AC7E782C9E #txt
hs0 f6 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f6 actionDecl 'ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData out;
' #txt
hs0 f6 actionTable 'out=in;
' #txt
hs0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>approve</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f6 83 243 26 26 -22 15 #rect
hs0 f6 @|RichDialogProcessStartIcon #fIcon
hs0 f7 guid 1655A7AD24E6D391 #txt
hs0 f7 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f7 actionDecl 'ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData out;
' #txt
hs0 f7 actionTable 'out=in;
' #txt
hs0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reject</name>
        <nameStyle>6,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f7 83 339 26 26 -15 15 #rect
hs0 f7 @|RichDialogProcessStartIcon #fIcon
hs0 f8 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f8 guid 1655A7AEBCF3C586 #txt
hs0 f8 339 243 26 26 0 12 #rect
hs0 f8 @|RichDialogEndIcon #fIcon
hs0 f9 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f9 guid 1655A7AF94EF7A89 #txt
hs0 f9 339 339 26 26 0 12 #rect
hs0 f9 @|RichDialogEndIcon #fIcon
hs0 f12 actionDecl 'ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData out;
' #txt
hs0 f12 actionTable 'out=in;
' #txt
hs0 f12 actionCode 'in.requestInformation.isApproved = true;' #txt
hs0 f12 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Approve leaving request</name>
        <nameStyle>23,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f12 152 234 144 44 -65 -8 #rect
hs0 f12 @|StepIcon #fIcon
hs0 f13 expr out #txt
hs0 f13 109 256 152 256 #arcP
hs0 f10 expr out #txt
hs0 f10 296 256 339 256 #arcP
hs0 f14 actionDecl 'ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData out;
' #txt
hs0 f14 actionTable 'out=in;
' #txt
hs0 f14 actionCode 'in.requestInformation.isApproved = false;' #txt
hs0 f14 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Reject leaving request</name>
        <nameStyle>22,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f14 168 330 128 44 -61 -8 #rect
hs0 f14 @|StepIcon #fIcon
hs0 f15 expr out #txt
hs0 f15 109 352 168 352 #arcP
hs0 f11 expr out #txt
hs0 f11 296 352 339 352 #arcP
hs0 f16 actionDecl 'ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData out;
' #txt
hs0 f16 actionTable 'out=in;
' #txt
hs0 f16 actionCode 'ivy.session.logoutSessionUser();' #txt
hs0 f16 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Logout session</name>
        <nameStyle>14,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f16 165 425 112 44 -43 -8 #rect
hs0 f16 @|StepIcon #fIcon
hs0 f17 guid 1655BDFC96B75EFF #txt
hs0 f17 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f17 actionDecl 'ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData out;
' #txt
hs0 f17 actionTable 'out=in;
' #txt
hs0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>logout</name>
        <nameStyle>6,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f17 83 434 26 26 -17 15 #rect
hs0 f17 @|RichDialogProcessStartIcon #fIcon
hs0 f18 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f18 499 435 26 26 0 12 #rect
hs0 f18 @|RichDialogProcessEndIcon #fIcon
hs0 f19 expr out #txt
hs0 f19 109 447 165 447 #arcP
hs0 f21 actionDecl 'ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData out;
' #txt
hs0 f21 actionTable 'out=in;
' #txt
hs0 f21 actionCode 'import ch.ivyteam.ivy.security.IRole;

in.userInformation.fullName = ivy.session.getSessionUser().getFullName();
in.userInformation.email = ivy.session.getSessionUser().getEMailAddress();
if (!ivy.session.getSessionUser().getAllRoles().isEmpty()) {
	for (IRole role : ivy.session.getSessionUser().getAllRoles()) {
		if (role.getName().equalsIgnoreCase("Superior")) {
			in.userInformation.role = role.getName();
			break;
		} else if (role.getName().equalsIgnoreCase("Employee")) {
			in.userInformation.role = role.getName();
			break;
		} else {
			in.userInformation.role = "";
		}	
	}
} else {
	in.userInformation.role = "";
}' #txt
hs0 f21 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Initiallize userInformation</name>
        <nameStyle>27,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f21 160 42 144 44 -68 -8 #rect
hs0 f21 @|StepIcon #fIcon
hs0 f22 expr out #txt
hs0 f22 109 64 160 64 #arcP
hs0 f23 actionDecl 'ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData out;
' #txt
hs0 f23 actionTable 'out=in;
' #txt
hs0 f23 actionCode 'import ch.ivyteam.ivy.workflow.TaskState;
import java.util.HashMap;
import ch.ivyteam.ivy.workflow.query.TaskQuery;
import ch.ivyteam.ivy.security.IUser;
import java.util.ArrayList;
import ch.ivyteam.ivy.workflow.ITask;

in.tasks = new ArrayList<String>();
in.tasksMap = new HashMap();

IUser user = ivy.session.getSessionUser();
TaskQuery query = TaskQuery.create().where().canWorkOn(user);

for (ITask task : ivy.wf.getTaskQueryExecutor().getResults(query)) {
	in.tasks.add(task.getName() + " (" + task.getStartTimestamp() + ")");
	in.tasksMap.put(task.getName() + " (" + task.getStartTimestamp() + ")", ivy.html.taskstartref(task));
}

in.currentTaskName = ivy.task.getName() + " (" + ivy.task.getStartTimestamp() + ")";' #txt
hs0 f23 security system #txt
hs0 f23 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Initiallize currentTaskName, tasks and tasksMap</name>
        <nameStyle>47,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f23 352 42 272 44 -132 -8 #rect
hs0 f23 @|StepIcon #fIcon
hs0 f24 expr out #txt
hs0 f24 304 64 352 64 #arcP
hs0 f2 expr out #txt
hs0 f2 624 64 659 64 #arcP
hs0 f25 actionDecl 'ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData out;
' #txt
hs0 f25 actionTable 'out=in;
' #txt
hs0 f25 actionCode 'import javax.faces.context.FacesContext;

FacesContext.getCurrentInstance().getExternalContext().redirect(ivy.html.startref("Start Processes/Process/start.ivp"));' #txt
hs0 f25 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Redirect to login page</name>
        <nameStyle>22,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f25 328 426 128 44 -60 -8 #rect
hs0 f25 @|StepIcon #fIcon
hs0 f26 expr out #txt
hs0 f26 277 447 328 448 #arcP
hs0 f20 expr out #txt
hs0 f20 456 448 499 448 #arcP
hs0 f27 guid 165604177A37A8C5 #txt
hs0 f27 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f27 method goToTask(String) #txt
hs0 f27 disableUIEvents false #txt
hs0 f27 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<java.lang.String taskName> param = methodEvent.getInputArguments();
' #txt
hs0 f27 inParameterMapAction 'out.targetTaskName=param.taskName;
' #txt
hs0 f27 outParameterDecl '<> result;
' #txt
hs0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>goToTask(String)</name>
        <nameStyle>16,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f27 83 531 26 26 -47 15 #rect
hs0 f27 @|RichDialogMethodStartIcon #fIcon
hs0 f28 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f28 339 531 26 26 0 12 #rect
hs0 f28 @|RichDialogProcessEndIcon #fIcon
hs0 f30 actionDecl 'ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData out;
' #txt
hs0 f30 actionTable 'out=in;
' #txt
hs0 f30 actionCode 'import javax.faces.context.FacesContext;

in.targetTaskPath = in.tasksMap.get(in.targetTaskName) as String;
FacesContext.getCurrentInstance().getExternalContext().redirect(in.targetTaskPath);' #txt
hs0 f30 type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
hs0 f30 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Go to target task</name>
        <nameStyle>17,7
</nameStyle>
    </language>
</elementInfo>
' #txt
hs0 f30 168 522 112 44 -44 -8 #rect
hs0 f30 @|StepIcon #fIcon
hs0 f31 expr out #txt
hs0 f31 109 544 168 544 #arcP
hs0 f29 expr out #txt
hs0 f29 280 544 339 544 #arcP
>Proto hs0 .type ivy.advanced.exercise3.HandleRequestDialog.HandleRequestDialogData #txt
>Proto hs0 .processKind HTML_DIALOG #txt
>Proto hs0 -8 -8 16 16 16 26 #rect
>Proto hs0 '' #fIcon
hs0 f3 mainOut f5 tail #connect
hs0 f5 head f4 mainIn #connect
hs0 f6 mainOut f13 tail #connect
hs0 f13 head f12 mainIn #connect
hs0 f12 mainOut f10 tail #connect
hs0 f10 head f8 mainIn #connect
hs0 f7 mainOut f15 tail #connect
hs0 f15 head f14 mainIn #connect
hs0 f14 mainOut f11 tail #connect
hs0 f11 head f9 mainIn #connect
hs0 f17 mainOut f19 tail #connect
hs0 f19 head f16 mainIn #connect
hs0 f0 mainOut f22 tail #connect
hs0 f22 head f21 mainIn #connect
hs0 f21 mainOut f24 tail #connect
hs0 f24 head f23 mainIn #connect
hs0 f23 mainOut f2 tail #connect
hs0 f2 head f1 mainIn #connect
hs0 f16 mainOut f26 tail #connect
hs0 f26 head f25 mainIn #connect
hs0 f25 mainOut f20 tail #connect
hs0 f20 head f18 mainIn #connect
hs0 f27 mainOut f31 tail #connect
hs0 f31 head f30 mainIn #connect
hs0 f30 mainOut f29 tail #connect
hs0 f29 head f28 mainIn #connect
