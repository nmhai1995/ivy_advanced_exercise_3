package main.com.axonactive.exercise.login;

import ch.ivyteam.ivy.environment.Ivy;

public class LoginHandler {
	
	public boolean checkLogin(String userName, String passWord)
	{
		return Ivy.session().loginSessionUser(userName, passWord);
	}
	
	
	
}
