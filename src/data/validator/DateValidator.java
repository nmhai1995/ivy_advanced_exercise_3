package data.validator;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import ch.ivyteam.ivy.environment.Ivy;

@FacesValidator(value="dateValidator")
public class DateValidator implements Validator {
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
	
		Date date = (Date)value;
		if (!date.after(new Date())) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, null, "Start date must be after current day"));
		}
	}
	
}
